<?php

namespace Flagbit\Locks;

use \SplFileObject;

class FileLock implements LockInterface
{
    /**
     * @var SplFileObject
     */
    private $file;

    public function __construct(SplFileObject $file)
    {
        $this->file = $file;
    }

    /**
     * Acquires the lock only if it is free at the time of invocation.
     *
     * @return bool TRUE if the lock was acquired and FALSE otherwise.
     */
    public function tryLock()
    {
        return $this->file->flock(LOCK_EX | LOCK_NB);
    }

    /**
     * Releases the lock.
     *
     * @return null
     */
    public function unlock()
    {
        $this->file->flock(LOCK_UN);
    }
}