<?php

namespace Flagbit\Locks;

interface LockInterface
{
    /**
     * Acquires the lock only if it is free at the time of invocation.
     *
     * @return bool TRUE if the lock was acquired and FALSE otherwise.
     */
    public function tryLock();

    /**
     * Releases the lock.
     *
     * @return null
     */
    public function unlock();
}